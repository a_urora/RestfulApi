﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RestfulApiV1.Data.Entity
{
    public class UserRole:BaseEntity
    {
        public int UsersId { get; set; }
        public int RolesId { get; set; }
        public Users Users { get; set; }
        public Roles Roles { get; set; }
    }
}
