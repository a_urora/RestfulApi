﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RestfulApiV1.Interface.Data
{
    public interface IRespository<T>
    {
        IQueryable<T> Table { get;  }

        T GetById(int id);

        void Add(T entity);

        void Add(IEnumerable<T> entities);

        void Delete(T entity);

        void Delete(IEnumerable<T> entities);

        void Update(T entity);

        void Update(IEnumerable<T> entities);
    }
}
