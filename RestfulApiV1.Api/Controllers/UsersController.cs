﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json.Linq;
using RestfulApiV1.Data.Entity;
using RestfulApiV1.Domain.Configure;
using RestfulApiV1.Domain.DTOs.Token;
using RestfulApiV1.Interface.Data;
using RestfulApiV1.Utils.Auth;
using RestfulApiV1.Utils.Json;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace RestfulApiV1.Api.Controllers
{
    [Route("api/[controller]")]
    //[Authorize]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private readonly IRespository<Users> _usersRespository;
        private TokenParameter _tokenParameter = new TokenParameter();

        public UsersController(IRespository<Users> usersRespository)
        {
            _usersRespository = usersRespository;

            var config = new ConfigurationBuilder()
                .SetBasePath(AppContext.BaseDirectory)
                .AddJsonFile("appsettings.json")
                .Build();

            _tokenParameter = config.GetSection("TokenParameter").Get<TokenParameter>();
        }

        /// <summary>
        /// 获取所有用户
        /// </summary>
        /// <returns></returns>
        // GET: api/<UsersController>
        [HttpGet]
        public string Get()
        {
            var list = _usersRespository.Table
                .Where(x => x.IsDeleted == false && x.IsActived == true)
                .Select(x => new { x.Id, x.Username, x.Remarks })
                .ToList();

            var res = new
            {
                Code = 1000,
                Data = list,
                Msg = "请求用户列表成功"
            };

            return JsonHelper.SerializeObject(res);
        }

        // GET api/<UsersController>/5
        [HttpGet("{id}")]
        public string Get(int id)
        {
            var user = _usersRespository.GetById(id);

            var res = new
            {
                Code = 1000,
                Data = user,
                Msg = "请求用户成功"
            };

            return JsonHelper.SerializeObject(res);
        }

        /// <summary>
        /// 增加用户
        /// </summary>
        /// <param name="user"></param>
        // POST api/<UsersController>
        [HttpPost]
        public string Post([FromBody]Users user)
        {
            var item = new Users
            {
                Username = user.Username,
                Remarks = user.Remarks
            };
            _usersRespository.Add(item);

            var res = new
            {
                Code = 1000,
                Data = item,
                Msg = "添加用户成功"
            };

            return JsonHelper.SerializeObject(res);
        }

        /// <summary>
        /// 修改用户信息
        /// </summary>
        /// <param name="id"></param>
        /// <param name="users"></param>
        // PUT api/<UsersController>/5
        [HttpPut("{id}")]
        public string Put(int id, Users users)
        {
            var item = _usersRespository.GetById(id);
            dynamic res;
            if (item != null)
            {
                item.Username = users.Username;
                item.Remarks = users.Remarks;
                _usersRespository.Update(item);

                res = new
                {
                    Code = 1000,
                    Data = item,
                    Msg = "修改用户信息成功"
                };
            }
            else
            {
                res = new
                {
                    Code = 1002,
                    Data = "",
                    Msg = "当前用户不存在"
                };
            }

            return JsonHelper.SerializeObject(res);
        }

        /// <summary>
        /// 删除用户
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        // DELETE api/<UsersController>/5
        [HttpDelete("{id}")]
        public string Delete(int id)
        {
            var item = _usersRespository.GetById(id);
            dynamic res;

            if(item != null)
            {
                item.IsDeleted = true;
                _usersRespository.Update(item);

                res = new
                {
                    Code = 1000,
                    Data = "",
                    Msg = "删除用户成功"
                };
            }
            else
            {
                res = new
                {
                    Code = 1002,
                    Data = "",
                    Msg = "当前用户不存在"
                };
            }

            return JsonHelper.SerializeObject(res);
        }



        /// <summary>
        /// 登录，以获取AccessToken
        /// </summary>
        /// <param name="loginInfo">登录信息，包括用户名和密码</param>
        /// <returns></returns>
        [HttpPost, Route("login")]
        [AllowAnonymous]
        public ActionResult Login([FromBody] LoginDTO loginInfo)
        {
            if (loginInfo.Username == null || loginInfo.Password == null)
            {
                return BadRequest("错误的请求");
            }

            var token =TokenHelper.GenerateToken(loginInfo.Username, "Admin",_tokenParameter);
            var refreshToken = "112358";
            return Ok(new {code=1000,data=new { token }, refreshToken,msg="登录成功" });
            //return Ok(new { accessToken = token, refreshToken });
        }

        /// <summary>
        /// 获取用户信息
        /// </summary>
        /// <param name="token"></param>
        /// <returns></returns>
        [HttpGet,Route("getUserInfo")]
        public ActionResult GetUserInfo(string token)
        {
            var accessList = new List<string>
            {
                "dashboard",
                "users",
                "roles",
                "setting"
            };

            var info = new
            {
                name = "admin",
                roles = "roles",
                avatar = "https://wpimg.wallstcn.com/f778738c-e4f8-4870-b634-56703b4acafe.gif",
                access=accessList
            };
            return Ok(new { code = 1000, data = info,  msg = "登录成功" });
            //return Ok(new { accessToken = token, refreshToken });
        }

        /// <summary>
        /// 登出
        /// </summary>
        /// <param name="loginInfo"></param>
        /// <returns></returns>
        public ActionResult Logout([FromBody] LoginDTO loginInfo)
        {
            if (loginInfo.Username == null || loginInfo.Password == null)
            {
                return BadRequest("错误的请求");
            }

            var token = TokenHelper.GenerateToken(loginInfo.Username, "Admin", _tokenParameter);
            var refreshToken = "112358";
            return Ok(new { code = 1000, data = new { token }, refreshToken, msg = "登录成功" });
            //return Ok(new { accessToken = token, refreshToken });
        }

        /// <summary>
        /// 使用RefreshToken，刷新AccessToken，避免重新登录获取AccessToken（有点Low不是吗）
        /// </summary>
        /// <param name="request">请求信息，包括AccessToken和RefreshToken</param>
        /// <returns></returns>
        [HttpPost, Route("refreshAccessToken")]
        [AllowAnonymous]
        public ActionResult RefreshAccessToken([FromBody] RefreshTokenDTO request)
        {
            if (request.AccessToken == null && request.RefreshToken == null)
            {
                return BadRequest("Invalid Request");
            }

            //这儿是验证Token的代码
            var handler = new JwtSecurityTokenHandler();
            try
            {
                ClaimsPrincipal claim = handler.ValidateToken(request.AccessToken, new TokenValidationParameters
                {
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_tokenParameter.Secret)),
                    ValidateIssuer = false,
                    ValidateAudience = false,
                    ValidateLifetime = false,
                }, out SecurityToken securityToken);
                var username = claim.Identity.Name;
                //这儿是生成Token的代码
                var token =TokenHelper.GenerateToken(username, "Admin",_tokenParameter);
                //此处的RefreshToken是直接指定和字符串，在正式项目中，应该是一个可验证的加密串，如md5、hash等
                var refreshToken = "654321";
                return Ok(new { accessToken = token, refreshToken });
            }
            catch (Exception)
            {
                return BadRequest("Invalid Request");
            }
        }
    }
}
